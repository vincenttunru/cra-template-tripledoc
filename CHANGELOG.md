# Changelog

This project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2020-02-27

### New features

- First version, shipping tripledoc-react, and an example component that uses the basic components and has full unit test coverage.
