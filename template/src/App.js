import React from 'react';
import { LoggedOut, LoggedIn } from 'tripledoc-react';
import { Connect } from './components/Connect';
import { Dashboard } from './components/Dashboard';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <React.StrictMode>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <p>
            <a
              className="App-link"
              href="https://reactjs.org"
              target="_blank"
              rel="noopener noreferrer"
            >
              Learn React
            </a>
            &nbsp;·&nbsp;
            <a
              className="App-link"
              href="https://vincenttunru.gitlab.io/tripledoc/"
              target="_blank"
              rel="noopener noreferrer"
            >
              Learn Tripledoc
            </a>
          </p>
          <LoggedIn>
            <Dashboard/>
          </LoggedIn>
          <LoggedOut>
            <Connect/>
          </LoggedOut>
        </header>
      </div>
    </React.StrictMode>
  );
}

export default App;
