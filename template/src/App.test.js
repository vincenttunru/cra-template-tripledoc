import React from 'react';
import { render, waitForDomChange } from '@testing-library/react';
import App from './App';

it('renders learn Tripledoc link', async () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/learn tripledoc/i);

  // Wait for the login status to be loaded:
  await waitForDomChange(linkElement);

  expect(linkElement).toBeInTheDocument();
});
