import React from 'react';
import SolidAuth from 'solid-auth-client';
import { useDocument } from 'tripledoc-react';
import { foaf } from 'rdf-namespaces';

export const Profile = (props) => {
  const [profileDoc] = useDocument(props.virtualDocuments.profileDoc);

  if (!profileDoc) {
    return null;
  }

  const profile = profileDoc.getSubject(props.virtualDocuments.webId);

  return (
    <div className="Profile">
      <p>
        Hello, {profile.getString(foaf.name) ?? props.virtualDocuments.webId}!
      </p>
      <p>
        <button className="Profile-logout" onClick={() => SolidAuth.logout()}>Log out</button>
      </p>
    </div>
  );
};
