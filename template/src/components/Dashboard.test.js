jest.mock('tripledoc-react', () => {
  const mockSubject = { getString: jest.fn(() => 'Arbitrary string') };
  const mockDocument = { getSubject: () => mockSubject };

  return {
    useWebId: jest.fn(() => undefined),
    useDocument: jest.fn(() => [mockDocument]),
  };
});

import React from 'react';
import { render } from '@testing-library/react';
import { Dashboard } from './Dashboard';

it('does not render when the user is logged out', async () => {
  const { queryByText } = render(<Dashboard />);

  expect(queryByText(/hello/i)).toBeNull();
});

it('renders a logout button when the user is logged in', async () => {
  const { useWebId } = jest.requireMock('tripledoc-react');
  useWebId.mockReturnValueOnce('https://arbitrary.webid');

  const { getByRole } = render(<Dashboard />);

  const logoutButton = getByRole('button');
  expect(logoutButton.textContent).toBe('Log out');
});
