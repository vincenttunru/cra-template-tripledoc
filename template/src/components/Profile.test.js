const mockSubject = { getString: jest.fn(() => 'Arbitrary string') };
const mockDocument = { getSubject: () => mockSubject };

jest.mock('tripledoc-react', () => {
  return {
    useDocument: jest.fn(() => [mockDocument]),
  };
});
jest.mock('solid-auth-client');

import React from 'react';
import { render } from '@testing-library/react';
import { Profile } from './Profile';

const defaultMockVirtualDocuments = {
  profileDoc: mockDocument,
  webId: 'https://arbitrary.webid',
};

it('does not render when the user is logged out', async () => {
  const { useDocument } = jest.requireMock('tripledoc-react');
  useDocument.mockReturnValueOnce([null])
  const { queryByText } = render(<Profile virtualDocuments={defaultMockVirtualDocuments}/>);

  expect(queryByText(/hello/i)).toBeNull();
});

it('renders a logout button when the user is logged in', async () => {
  const { getByRole } = render(<Profile virtualDocuments={defaultMockVirtualDocuments}/>);

  const logoutButton = getByRole('button');
  expect(logoutButton.textContent).toBe('Log out');
});

it('shows the name of the current user if available', async () => {
  const { useDocument } = jest.requireMock('tripledoc-react');
  const mockSubject = useDocument()[0].getSubject();
  mockSubject.getString.mockReturnValueOnce('Vincent')

  const { queryByText } = render(<Profile virtualDocuments={defaultMockVirtualDocuments}/>);

  expect(queryByText(/Vincent/)).not.toBeNull();
});

it('shows the WebID of the current user if no name is available', async () => {
  const { useDocument } = jest.requireMock('tripledoc-react');
  const mockSubject = useDocument()[0].getSubject();
  mockSubject.getString.mockReturnValueOnce(null)

  const mockVirtualDocuments = {
    ...defaultMockVirtualDocuments,
    webId: 'https://some.webid',
  };
  const { queryByText } = render(<Profile virtualDocuments={mockVirtualDocuments}/>);

  expect(queryByText(/some.webid/)).not.toBeNull();
});

it('should log out when the logout button is clicked', async () => {
  const { logout } = jest.requireMock('solid-auth-client');
  const { getByRole } = render(<Profile virtualDocuments={defaultMockVirtualDocuments}/>);

  const logoutButton = getByRole('button');
  logoutButton.click();

  expect(logout.mock.calls.length).toBe(1);
});
