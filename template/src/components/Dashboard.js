import React from 'react';
import { useWebId } from 'tripledoc-react';
import { describeDocument } from 'plandoc';
import { Profile } from './Profile';

export const Dashboard = () => {
  const webId = useWebId();

  if (!webId) {
    return null;
  }

  const virtualDocuments = getVirtualDocuments(webId);

  return (
    <Profile virtualDocuments={virtualDocuments}/>
  );
};

function getVirtualDocuments(webId) {
  const profileDoc = describeDocument().isFoundAt(webId);

  return {
    webId,
    profileDoc,
  };
}
