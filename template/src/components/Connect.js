import React from 'react';
import SolidAuth from 'solid-auth-client';

export const Connect = () => {
  const [idp, setIdp] = React.useState('');

  const onSubmit = (event) => {
    event.preventDefault();

    SolidAuth.login(idp);
  };

  return (
    <>
      <form onSubmit={onSubmit} className="Connect">
        <label htmlFor="idp" className="Connect-label">Connect to your Pod:</label>
        <input
          type="url"
          value={idp}
          onChange={(e) => setIdp(e.target.value)}
          id="idp"
          name="idp"
          list="idps"
          className="Connect-input"
          placeholder="https://inrupt.net"
          required={true}
        />
        <datalist id="idps">
          <option value="https://inrupt.net"/>
          <option value="https://solid.community"/>
        </datalist>
        <button type="submit" className="Connect-submit">Connect</button>
      </form>
    </>
  );
};
