jest.mock('solid-auth-client');

import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import { Connect } from './Connect';

it('renders the Pod connection form', async () => {
  const { asFragment } = render(<Connect/>);

  expect(asFragment()).toMatchSnapshot();
});

it('should log you in when the form is submitted', async () => {
  const { login } = jest.requireMock('solid-auth-client');
  const { getByRole } = render(<Connect/>);

  const idpField = getByRole('textbox');
  const loginButton = getByRole('button');
  fireEvent.change(idpField, { target: { value: 'https://some.idp' } })
  loginButton.click();

  expect(login.mock.calls.length).toBe(1);
  expect(login.mock.calls[0]).toEqual(['https://some.idp']);
});
