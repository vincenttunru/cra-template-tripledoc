This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) using the [Tripledoc template](https://gitlab.com/vincenttunru/cra-template-tripledoc).

Some useful resources:

- [Create React App documentation](https://create-react-app.dev/docs/folder-structure).
- [Writing a Solid web app](https://solidproject.org/for-developers/apps/first-app)
- [Tripledoc](https://vincenttunru.gitlab.io/tripledoc/) (for manipulating data)
- [Plandoc](https://www.npmjs.com/package/plandoc) (for setting up your data model)

Additionally, it is strongly recommended to [add TypeScript support](https://create-react-app.dev/docs/adding-typescript) if you are familiar with TypeScript. When working with Solid, you cannot make assumptions about whether data exists or not, and TypeScript will help you catch most of the potential errors that flow from that.
