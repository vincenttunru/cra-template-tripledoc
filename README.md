cra-template-tripledoc
======
A template for [Create React App](https://create-react-app.dev/) that initialises it to work with [Tripledoc](https://vincenttunru.gitlab.io/tripledoc/).

# Installation

```bash
npx create-react-app my-app --template tripledoc
```

# Changelog

See [CHANGELOG](https://gitlab.com/vincenttunru/cra-template-tripledoc/blob/master/CHANGELOG.md).

# License

MIT © [Inrupt](https://inrupt.com)
